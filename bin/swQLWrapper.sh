#!/bin/bash

for last; do true; done

mkdir -p "$last/post_processing/QL/"

swQL.py "$@" > "$last/post_processing/QL/QL.log" 2>&1
status=$?
echo $status > "$last/post_processing/QL/QL.status"

swQL.py -v > "$last/post_processing/QL/QL.version"

cat "$last/post_processing/QL/QL.log"

exit $status