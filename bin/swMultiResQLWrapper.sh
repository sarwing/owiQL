#!/bin/bash

inDir=$1
res=$2

mkdir -p "$inDir/post_processing/L2MQL$res/"

swQL.py -i $inDir -d "$inDir/post_processing/L2MQL$res/" -r $res > "$inDir/post_processing/L2MQL$res/L2MQL.log" 2>&1
status=$?
echo $status > "$inDir/post_processing/L2MQL$res/L2MQL.status"

swQL.py -v > "$inDir/post_processing/L2MQL$res/L2MQL.version"

cat "$inDir/post_processing/L2MQL$res/L2MQL.log"

exit $status