#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
from builtins import str
import logging
import argparse
import datetime
import sys
import socket
import os

logger = logging.getLogger(os.path.basename(__file__))

description = """Generates quicklooks images for concatenated netCDF files."""
parser = argparse.ArgumentParser(description=description)

parser.add_argument(
    "--debug", action="store_true", default=False, help="start the script in debug mode"
)
parser.add_argument(
    "-i",
    "--inDir",
    action="store",
    help="Input directory. Will also be used as root path for the output directory. (which is /post_processing/QL)",
)
parser.add_argument(
    "--in-file",
    action="store",
    help="Input file. Will also be used as root path for the output directory. (which is /post_processing/QL)",
)
parser.add_argument(
    "-d",
    "--outDir",
    action="store",
    default="",
    help="Output directory. If none is provided, inDir/post_processings/QL is used as outDir",
)
parser.add_argument(
    "-v",
    "--version",
    action="store_true",
    default=False,
    help="Returns the script version",
)
parser.add_argument(
    "-r",
    "--resolution",
    action="store",
    nargs="?",
    type=int,
    default=-1,
    help="Activates QL for multiRes and indicates which owi file to chose (if 5 is given, the owi file containing data for resolution of 5km will be processed",
)

try:
    args = parser.parse_args()

    if args.debug:
        logging.basicConfig(
            level=logging.DEBUG,  # Set the logging level (DEBUG, INFO, WARNING, ERROR, CRITICAL)
            format="%(asctime)s [%(levelname)s]: %(message)s",  # Define the log message format
            datefmt="%Y-%m-%d %H:%M:%S",  # Define the date/time format
        )
        logger = logging.getLogger(os.path.basename(__file__))
        try:
            import debug
        except:
            pass
    else:
        logging.basicConfig(
            level=logging.INFO,  # Set the logging level (DEBUG, INFO, WARNING, ERROR, CRITICAL)
            format="%(asctime)s [%(levelname)s]: %(message)s",  # Define the log message format
            datefmt="%Y-%m-%d %H:%M:%S",  # Define the date/time format
        )
        logger = logging.getLogger(os.path.basename(__file__))

    import owiQL

    if args.version:
        print(owiQL.version)
        sys.exit(0)

    logger.info("Script executed at %s" % datetime.datetime.now())
    logger.info("Called with the following command : %s" % " ".join(sys.argv))
    logger.info("The host machine is : %s" % socket.gethostname())

    if not args.inDir and not args.in_file:
        parser.error("One of -i or --in-file must be provided.")

    owiQL.createQL(
        args.in_file if args.in_file else args.inDir, args.outDir, args.resolution
    )
except Exception as e:
    logger.exception("Exception : %s", str(e))
    sys.exit(1)
