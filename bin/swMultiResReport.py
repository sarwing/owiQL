#!/usr/bin/env python

from builtins import str
import owiQL
import logging
import argparse
import datetime
import sys
import socket
import os

logging.basicConfig(format='%(asctime)s %(messages)s')
logger = logging.getLogger(os.path.basename(__file__))
logger.setLevel(logging.INFO)

logger.info("Script executed at %s" % datetime.datetime.now())
logger.info("Called with the following command : %s" % " ".join(sys.argv))
logger.info("The host machine is : %s" % socket.gethostname())

description = """Generates multi-resolution report"""
parser = argparse.ArgumentParser(description = description)

parser.add_argument("--debug", action="store_true", default=False, help="start the script in debug mode")
parser.add_argument("--resReport", action="store_true", default=False, help="Generates multires report summary HTML page based on last multiResReport.csv")
parser.add_argument("-m", "--resReportXarg", action="store", nargs='+', type=int, help="Generates multires report summary HTML page for one directory for the given resolutions. Only works with -i, and -c")
parser.add_argument("-c", "--comments", action="store", default="", nargs='*', help="Storm comments")
parser.add_argument("-i", "--inDir", action="store", help="Input directory. Will also be used as root path for the output directory. (which is /post_processing/QL)")

try:
    args = parser.parse_args()
    if args.debug:
        try:
            import debug
        except:
            pass
        
    if args.resReport:
        owiQL.multiResReport(args.inDir)
        sys.exit(0)
        
    if args.resReportXarg:
        owiQL.generateMultiResSummary({'path': args.inDir, 'comments': " ".join(args.comments)}, args.resReportXarg)
        sys.exit(0)
except Exception as e:
    logger.exception("Exception : %s", str(e) )
    sys.exit(1)