#!/usr/bin/env python
from __future__ import division
from future import standard_library

standard_library.install_aliases()
from builtins import map
from builtins import zip
from builtins import str
from past.utils import old_div

template = """
<!DOCTYPE html>
<html>
    <head>
        <style>
            .fixed
            {
                font-family:    monospace, courrier;
                font-size:      10px;
            }
            table {
              border-collapse: collapse;
            }
            th,td {
                border: 1px solid #888;
                padding: 0.25em 0.5em; 
            }
            th.separator,td.separator {
                margin: 1em;
                border: none;
            }
        </style>
        <script>
        function dispDetails()
        {
            details = document.getElementById("details");
            if (details.style.display == "none")
            {
                details.style.display = "block";
            }
            else
            {
                details.style.display = "none";            
            }
        }
        </script>
    </head>
    <body>
        <h1>{{title}}</h1>
        {% for var in htmlDict['normal'] %}
            {% if '\n' in var %}
                <br/>
            {% else %}
                <div style="display: inline-block; width: {{infoDict['pngWidth']}}%; margin-right: 1%; margin-bottom: 0.5em;">
                    <a href="{{htmlDict['normal'][var]['png']}}">
                        <h4 style="text-align: center">{{htmlDict['normal'][var]['title']}}</h4>
                        <img src="{{htmlDict['normal'][var]['png_small']}}" width="100%">
                    </a>
                </div>
            {% endif %}
        {% endfor %}
        <p style="clear: both;">
        <button onclick="dispDetails()" style="font-size: 2.5em;">Display details</button> 
        <div id="details" style="display: none;">
         {% for var in htmlDict['detailed'] %}
            {% if '\n' in var %}
                <br/>
            {% else %}
                <div style="display: inline-block; width: {{infoDict['pngWidth']}}%; margin-right: 1%; margin-bottom: 0.5em;">
                    <a href="{{htmlDict['detailed'][var]['png']}}">
                        <h4 style="text-align: center">{{htmlDict['detailed'][var]['title']}}</h4>
                        <img src="{{htmlDict['detailed'][var]['png_small']}}" width="100%">
                    </a>
                </div>
            {% endif %}
        {% endfor %}
        </div>
    </body>
</html>
"""
import sys
import os

import matplotlib as mpl

mpl.use("Agg")  # no display

import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np
import owi
import glob
import datetime
import jinja2
import collections
from pathurl import toUrl
import base64
import io
import textwrap
import logging
from shapely.wkt import loads
from shapely.geometry import Polygon
import csv
import colormap_ext
import shapely.geometry as geometry
import matplotlib.patches as patches
import cartopy.crs as ccrs
import cartopy.feature as cfeature
import matplotlib.ticker as mticker

from ._version import __version__

version = __version__

logging.basicConfig(format="%(asctime)s %(messages)s")
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

# import pdb

# sys.path.append('/home3/homedir7/perso/oarcher/sarwing/losafe/users/oarcher/LOP/bin')

hiDpi = 600
loDpi = 100


mainVars = [
    "\n",
    "owiWindSpeed",
    "owiWindSpeed_co",
    "owiWindSpeed_cross",
    #'owiInversionTables_UV/owiWindSpeed_Tab_dualpol_2steps',
    "owiWindSpeed_IPF",
    "\n1",
    # "owiWindDirection",
    # "owiWindDirection_co",
    # "owiWindDirection_cross",
    #'owiInversionTables_UV/owiWindDirection_Tab_copol',
    #'owiPreProcessing/estimWindDir_2',
    "owiPreProcessing/wind_streaks_orientation",
    "owiPreProcessing/estimWindDir_2_cross",
    "owiPreProcessing/estimWindDir_2",
    "\n2",
    "owiAncillaryWindSpeed",
    # "owiAncillaryWindDirection",
    "\n3",
    "owiPreProcessing/ND_2",
    "owiPreProcessing/ND_2_cross",
    "\n4",
    "owiWindFilter",
    "owiWindQuality",
    "owiPreProcessing/wind_streaks_orientation_stddev",
    "owiPreProcessing/Filter_1",
    "owiPreProcessing/Filter_2",
    "owiPreProcessing/Filter_3",
    "owiPreProcessing/Filter_1_cross",
    "owiPreProcessing/Filter_2_cross",
    "owiPreProcessing/Filter_3_cross",
]

detailedVars = [
    "owiNrcs_no_noise_correction",
    "owiNrcs",
    "owiMask_Nrcs",
    "\n5" "owiNrcs_detrend",
    "owiNrcs_detrend_cross",
    "owiNrcs_detrend_cross_flattened",
    "\n6",
    "owiNrcs_cross_no_noise_correction",
    "owiNrcs_cross",
    "owiMask_Nrcs_cross",
    "\n7",
    "owiNesz",
    "owiNesz_cross",
    "\n8",
    "owiSdig",
    "owiDsig_cross",
    "\n9",
    "sliceNumber",
]

skipVars = [
    "owiElevationAngle",
    "owiHeading",
    "owiIncidenceAngle",
    "owiInversionQuality",
    "owiLandFlag",
    "owiLat",
    "owiLon",
    # "owiMask",
    #    'owiNesz',
    #    'owiNesz_cross',
    #    'owiNrcs',
    "owiNrcsCmod",
    #    'owiNrcs_cross',
    #    'owiNrcs_cross_no_noise_correction',
    #    'owiNrcs_no_noise_correction',
    "owiPBright",
    "owiPreProcessing/CoefMixWindDir_2",
    "owiPreProcessing/CoefMixWindDir_2_cross",
    "owiPreProcessing/FilterBinary_1",
    "owiPreProcessing/FilterBinary_1_cross",
    "owiPreProcessing/FilterBinary_2",
    "owiPreProcessing/FilterBinary_2_cross",
    "owiPreProcessing/FilterBinary_3",
    "owiPreProcessing/FilterBinary_3_cross",
    "owiPreProcessing/Filter_1",
    "owiPreProcessing/Filter_1_cross",
    "owiPreProcessing/Filter_2",
    "owiPreProcessing/Filter_2_cross",
    "owiPreProcessing/Filter_3",
    "owiPreProcessing/Filter_3_cross",
    #    'owiPreProcessing/MixWindDir_2',
    #    'owiPreProcessing/MixWindDir_2_cross',
    "owiPreProcessing/NiceDisplay_1",
    "owiPreProcessing/NiceDisplay_1_cross",
    "owiPreProcessing/PicPrincp_2",
    "owiPreProcessing/PicPrincp_2_cross",
    "owiPreProcessing/contrast_2",
    "owiPreProcessing/contrast_2_cross",
    "owiPreProcessing/diffVoisins_2",
    "owiPreProcessing/diffVoisins_2_cross",
    "owiPreProcessing/estimWindDir_2",
    "owiPreProcessing/estimWindDir_2_cross",
    "owiPreProcessing/pourcentPointUtil_2",
    "owiPreProcessing/pourcentPointUtil_2_cross",
    "owiPreProcessing/rapportPic_2",
    "owiPreProcessing/rapportPic_2_cross",
    # "owiWindQuality",
]


def haversine(lon1, lat1, lon2, lat2):
    """
    Calculate the great circle distance and bearing between two points on the earth.

    Parameters:
    lon1 (float): Longitude of the first point in decimal degrees.
    lat1 (float): Latitude of the first point in decimal degrees.
    lon2 (float): Longitude of the second point in decimal degrees.
    lat2 (float): Latitude of the second point in decimal degrees.

    Returns:
    tuple: A tuple containing:
        - float: Great circle distance between the two points in kilometers.
        - float: Bearing from the first point to the second point in degrees.

    """
    # convert decimal degrees to radians
    lon1, lat1, lon2, lat2 = list(map(np.radians, [lon1, lat1, lon2, lat2]))

    # haversine formula
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = (
        np.sin(old_div(dlat, 2)) ** 2
        + np.cos(lat1) * np.cos(lat2) * np.sin(old_div(dlon, 2)) ** 2
    )
    c = 2 * np.arcsin(np.sqrt(a))
    r = 6371  # Radius of earth in kilometers. Use 3956 for miles
    bearing = np.arctan2(
        np.sin(lon2 - lon1) * np.cos(lat2),
        np.cos(lat1) * np.sin(lat2) - np.sin(lat1) * np.cos(lat2) * np.cos(lon2 - lon1),
    )
    return c * r, np.rad2deg(bearing)


def polar2uv(rho, theta):
    u = rho * np.cos(np.deg2rad(360 - theta - 90))
    v = rho * np.sin(np.deg2rad(360 - theta - 90))
    return u, v


def niceMap(*args, **kwargs):
    projection = kwargs.get("projection", "PlateCarree")

    if projection == "cyl":
        proj = ccrs.PlateCarree()
    elif projection == "stere":
        proj = ccrs.Stereographic(
            central_longitude=kwargs.get("lon_0", 0),
            central_latitude=kwargs.get("lat_0", 0),
        )
    else:
        # Handle other projections if needed
        proj = ccrs.PlateCarree()  # default

    fig, ax = plt.subplots(figsize=(10, 10), subplot_kw={"projection": proj})

    # Set extent
    if all(k in kwargs for k in ("llcrnrlon", "llcrnrlat", "urcrnrlon", "urcrnrlat")):
        lon_min = kwargs["llcrnrlon"]
        lat_min = kwargs["llcrnrlat"]
        lon_max = kwargs["urcrnrlon"]
        lat_max = kwargs["urcrnrlat"]
        ax.set_extent([lon_min, lon_max, lat_min, lat_max], crs=ccrs.PlateCarree())
    elif all(k in kwargs for k in ("width", "height", "lon_0", "lat_0")):
        # Compute extent based on center and width/height in degrees
        lon_0 = kwargs["lon_0"]
        lat_0 = kwargs["lat_0"]
        width = kwargs["width"]
        height = kwargs["height"]
        lon_min = lon_0 - width / 2
        lon_max = lon_0 + width / 2
        lat_min = lat_0 - height / 2
        lat_max = lat_0 + height / 2
        ax.set_extent([lon_min, lon_max, lat_min, lat_max], crs=ccrs.PlateCarree())
    else:
        # Default extent
        ax.set_global()
        lon_min, lon_max = -180, 180
        lat_min, lat_max = -90, 90

    # Draw coastlines and fill continents
    ax.coastlines(resolution="110m", color="coral", zorder=5)
    ax.add_feature(cfeature.LAND.with_scale("110m"), facecolor="coral", zorder=0)

    # Draw gridlines (parallels and meridians)
    gl = ax.gridlines(
        crs=ccrs.PlateCarree(),
        draw_labels=True,
        linewidth=1,
        color="gray",
        alpha=0.5,
        linestyle="--",
        zorder=5,
    )
    gl.top_labels = False
    gl.right_labels = False
    gl.xlabel_style = {"size": 5}
    gl.ylabel_style = {"size": 5}

    # Set gridline intervals
    parallels = np.arange(np.floor(lat_min - 1), np.ceil(lat_max + 1), 2)
    meridians = np.arange(np.floor(lon_min - 1), np.ceil(lon_max + 1), 2)

    gl.xlocator = mticker.FixedLocator(meridians)
    gl.ylocator = mticker.FixedLocator(parallels)

    return fig, ax


def add_polygon(ax, polygon):
    if not isinstance(polygon, geometry.Polygon):
        polygon = geometry.Polygon(polygon)

    exterior_coords = polygon.exterior.coords
    patch = patches.Polygon(
        exterior_coords, closed=True, edgecolor="blue", facecolor="none", alpha=1
    )
    ax.add_patch(patch)

    for interior in polygon.interiors:
        interior_coords = interior.coords
        patch = patches.Polygon(
            interior_coords, closed=True, edgecolor="blue", facecolor="none", alpha=1
        )
        ax.add_patch(patch)


def boundingQL(filename, title, owiFile, htmlDict, outDir, lon0, lat0, width, height):
    logger.debug("Creating bounding box QL...")

    pngFile = (
        f"{outDir}/{os.path.splitext(os.path.basename(owiFile))[0]}-{filename}.png"
    )
    htmlDict["normal"][title] = collections.OrderedDict({})

    if not owi.getAttribute(owiFile, "slicesFootprints") is None:
        footprints = owi.getAttribute(owiFile, "slicesFootprints").split(";")
    elif not owi.getAttribute(owiFile, "footprint") is None:
        footprints = [owi.getAttribute(owiFile, "footprint")]
    else:
        logger.warning("No footprints found in OWI file.")
        return
    logger.debug(f"footprints: {footprints}")

    # Set up projection
    proj = ccrs.Stereographic(central_longitude=lon0, central_latitude=lat0)

    fig, ax = plt.subplots(figsize=(10, 10), subplot_kw={"projection": proj})

    # Compute extent
    lon_min = lon0 - width / 2
    lon_max = lon0 + width / 2
    lat_min = lat0 - height / 2
    lat_max = lat0 + height / 2

    ax.set_extent([lon_min, lon_max, lat_min, lat_max], crs=ccrs.PlateCarree())

    # Draw coastlines and fill continents
    ax.coastlines(resolution="110m", color="coral", zorder=5)
    ax.add_feature(cfeature.LAND.with_scale("110m"), facecolor="coral", zorder=0)

    # Draw gridlines
    gl = ax.gridlines(
        crs=ccrs.PlateCarree(),
        draw_labels=True,
        linewidth=1,
        color="gray",
        alpha=0.5,
        linestyle="--",
        zorder=5,
    )
    gl.top_labels = False
    gl.right_labels = False
    gl.xlabel_style = {"size": 5}
    gl.ylabel_style = {"size": 5}

    # Set gridline intervals
    parallels = np.arange(np.floor(lat_min - 1), np.ceil(lat_max + 1), 2)
    meridians = np.arange(np.floor(lon_min - 1), np.ceil(lon_max + 1), 2)

    gl.xlocator = mticker.FixedLocator(meridians)
    gl.ylocator = mticker.FixedLocator(parallels)

    # Plot footprints
    for footprint in footprints:
        shp = loads(footprint)
        ax.add_geometries(
            [shp], crs=ccrs.PlateCarree(), facecolor="none", edgecolor="blue"
        )

    htmlDict["normal"][title]["png"] = os.path.basename(pngFile)
    htmlDict["normal"][title]["title"] = title

    plt.savefig(pngFile, dpi=300)
    ioBytes = io.BytesIO()
    plt.savefig(ioBytes, format="png", dpi=100)
    ioBytes.seek(0)
    htmlDict["normal"][title]["png_small"] = (
        b"data:image/png;base64," + base64.b64encode(ioBytes.read())
    ).decode("utf-8")
    plt.close()

    logger.debug("Finished generating bounding box QL.")


def checkVersion(versionPath):
    try:
        logger.debug("Trying to open version file %s ..." % versionPath)
        f = open(versionPath, "r")
    except IOError:
        logger.debug("No previous version file.")
        return True
    else:
        with f:
            ver = f.readline().strip()
            if ver == version:
                logger.debug("Version file open, versions are the same : %s." % ver)
                return False
            else:
                logger.debug(
                    "Version file open, versions are different, previous : %s, newest : %s. Generation will be forced."
                    % (ver.strip(), version.strip())
                )
                return True


def checkStatus(statusPath):
    try:
        logger.debug("Trying to open status file %s ..." % statusPath)
        f = open(statusPath, "r")
    except IOError:
        logger.debug("No previous status file.")
        return True
    else:
        with f:
            status = f.readline().strip()
            if status == "0":
                logger.debug("Status OK")
                return False
            else:
                logger.debug("Status is %s." % status)
                return True


def multiResReport(reportPath):
    reportDirs = []
    reportRes = []
    with open(reportPath + "/multiResReport.csv", "r") as csvReport:
        reportDict = csv.DictReader(csvReport)
        for row in reportDict:
            dirPath = os.path.dirname(row["L2M file"])
            res = row["Resolution"]
            comments = row["Comments"]
            statusFile = "%s/L2M%s.status" % (dirPath, res)
            if os.path.isfile(statusFile):
                with open(statusFile, "r") as status:
                    code = status.readline().strip()
                    if code == "0":
                        pathCommentDict = {"path": dirPath, "comments": comments}
                        if pathCommentDict not in reportDirs:
                            reportDirs.append(pathCommentDict)
                        if res not in reportRes:
                            reportRes.append(int(res))

        logger.info("Building multiRes summary reports...")
        for dictPath in reportDirs:
            generateMultiResSummary(dictPath, reportRes)


def generateMultiResSummary(pathCommentDict, resolutions):
    inDir = pathCommentDict["path"]
    comments = pathCommentDict["comments"]

    htmlDict = {}
    htmlDict["detailed"] = collections.OrderedDict({})
    htmlDict["normal"] = collections.OrderedDict({})

    selectRes = []
    for res in resolutions:
        statusPath = "%s/post_processing/L2MQL%s/L2MQL.status" % (inDir, res)
        if not checkStatus(statusPath):  # Status OK
            selectRes.append(res)

    if len(selectRes) > 0:
        infoDict = {}
        if len(selectRes) < 5:
            infoDict["pngWidth"] = (100.0 / len(selectRes)) - 2
        else:
            infoDict["pngWidth"] = (100.0 / 5) - 2

        if infoDict["pngWidth"] > 50:
            infoDict["pngWidth"] = 50

        for var in mainVars + detailedVars:
            detailed = "normal"
            if var in detailedVars:
                detailed = "detailed"

            for res in selectRes:
                groupName = os.path.dirname(var)

                if "\n" in var:
                    continue

                varRes = "%s%s" % (var, res)

                cleanVar = var.replace("/", "-")
                currPath = "%s/post_processing/L2MQL%s" % (inDir, res)
                # logger.info("%s/*%s.png" % (currPath, cleanVar))
                pngFiles = glob.glob("%s/*%s.png" % (currPath, cleanVar))
                if len(pngFiles) > 0:
                    logger.debug("Found image for var %s at res %s" % (var, res))
                    htmlDict[detailed][varRes] = {}
                    htmlDict[detailed][varRes]["png"] = "L2MQL%s/%s" % (
                        res,
                        os.path.basename(pngFiles[0]),
                    )
                    htmlDict[detailed][varRes]["title"] = "%s at resolution : %s" % (
                        var.replace("%s/" % groupName, ""),
                        res,
                    )
                    try:
                        img = mpimg.imread(pngFiles[0])
                    except Exception as e:
                        logger.error("reading %s : %s" % (pngFiles[0], str(e)))
                        continue
                    imgplot = plt.imshow(img)
                    # plt.tight_layout()
                    plt.axis("off")
                    imgplot.axes.get_xaxis().set_visible(False)
                    imgplot.axes.get_yaxis().set_visible(False)
                    ioBytes = io.BytesIO()
                    plt.savefig(
                        ioBytes,
                        format="png",
                        dpi=loDpi,
                        bbox_inches="tight",
                        pad_inches=0,
                    )
                    ioBytes.seek(0)
                    htmlDict[detailed][varRes]["png_small"] = (
                        b"data:image/png;base64, " + base64.b64encode(ioBytes.read())
                    ).decode("utf-8")
                    plt.close()
                else:
                    logger.warning(
                        "Cannot find image for var %s at res %s" % (var, res)
                    )
            lineRet = "\nres" + var
            htmlDict[detailed][lineRet] = {}

        title = "Multiresolution summary report for storm : %s" % (comments)

        t = jinja2.Template(template)
        html = t.render(htmlDict=htmlDict, infoDict=infoDict, title=title)
        reportFname = "%s/post_processing/multiResSummary.html" % (inDir)
        reportFid = open(reportFname, "w")
        reportFid.write(html)
        reportFid.close()
        logger.info("Summary multiRes HTML file: %s" % toUrl(reportFname, schema="dmz"))
    else:
        logger.warning("No L2M QL available. Summary report can't be generated.")


def createQL(inDir, outDir="", resolution=-1):
    import os

    if os.path.isfile(inDir):
        owiFile = inDir
        logger.info("Starting QL generation for input file: %s", inDir)
    else:
        logger.info("Starting QL generation for input directory: %s", inDir)
        if outDir == "":
            if resolution == -1:
                outDir = inDir + "/post_processing/QL"
            else:
                outDir = inDir + "/post_processing/L2MQL" + resolution

        if resolution == -1:
            statusPath = outDir + "/QL.status"
        else:
            versionPath = outDir + "/L2MQL.version"
            statusPath = outDir + "/L2MQL.status"

        if not os.path.exists(outDir):
            try:
                os.makedirs(outDir)
            except:
                pass

        if resolution == -1:
            try:
                owiFile = glob.glob("%s/*owi*.nc" % inDir)[0]
            except:
                logger.error("no owi files found : %s/*owi*.nc\n" % inDir)
                sys.exit(1)
        else:
            try:
                owiFiles = glob.glob("%s/*owi*.nc" % inDir)
            except:
                logger.error("no owi files found : %s/*owi*.nc\n" % inDir)
                sys.exit(1)
            for owiF in owiFiles:
                res = int(owiF.split("-")[-2])  # Remove the front 0
                if res == resolution:
                    owiFile = owiF
                    break

    try:
        date = datetime.datetime.strptime(
            os.path.basename(owiFile).split("-")[4], "%Y%m%dt%H%M%S"
        )
        strdate = date.strftime("%c")
        strOwi = os.path.basename(owiFile)
    except:
        strdate = "??"
        strOwi = os.path.basename(owiFile)

    title = "%s (%s)" % (strOwi, strdate)
    infoDict = {}
    infoDict["pngWidth"] = 20

    owiData = owi.readFile(owiFile)
    owiMeta = owi.readMeta(owiFile)

    if "hasGap" in owiMeta:
        hasGap = owiMeta["hasGap"] == "True"
    else:
        hasGap = False
    if hasGap:
        logger.warning("File has a gap, using hexbin instead of pcolormesh")

    htmlDict = collections.OrderedDict({})
    owiLon = owiData["owiLon"]
    owiLat = owiData["owiLat"]

    mask = np.ma.getmask(np.ma.masked_not_equal(owiData["owiMask"], 0))

    owiLonMask = np.ma.array(owiData["owiLon"], mask=mask)
    owiLatMask = np.ma.array(owiData["owiLat"], mask=mask)

    hasNan = False
    if np.isnan(owiLon).any():
        logger.warning("File has nan values, using pcolor instead of pcolormesh")
        hasNan = True

    # lonMin=np.min(owiLonMask)
    # lonMax=np.max(owiLonMask)
    # latMin=np.min(owiLatMask)
    # latMax=np.max(owiLatMask)

    lonMin = np.nanmin(owiLon) - 2
    lonMax = np.nanmax(owiLon) + 2
    latMin = np.nanmin(owiLat) - 0.3
    latMax = np.nanmax(owiLat) + 0.3

    logger.debug(
        "lonMin %s latMin %s lonMax %s latMax %s " % (lonMin, latMin, lonMax, latMax)
    )

    # ijcenter=tuple(np.array(np.shape(owiLat))/2)
    # lon_0=owiLon[ijcenter] #Not valid because the point could be invalid
    # lat_0=owiLat[ijcenter] #Not valid because the point could be invalid
    lon_0 = old_div((lonMax + lonMin), 2)
    lat_0 = old_div((latMax + latMin), 2)
    # owiLonValid = owiLonMask[~owiLonMask.mask]
    # owiLatValid = owiLatMask[~owiLatMask.mask]
    # imsize=geo.haversine(owiLonValid.compressed()[0],owiLatValid.compressed()[0],owiLonValid.compressed()[-1],owiLatValid.compressed()[-1])[0]*1000
    width = lonMax - lonMin
    height = latMax - latMin
    cmap = plt.get_cmap("gray")
    htmlDict["detailed"] = collections.OrderedDict({})
    htmlDict["normal"] = collections.OrderedDict({})

    widthLarge = (lonMax + 35) - (lonMin - 35)
    heightLarge = (latMax + 15) - (latMin - 15)

    boundingQL(
        "large-bounding-box",
        "BoundingBox_Large",
        owiFile,
        htmlDict,
        outDir,
        lon_0,
        lat_0,
        widthLarge,
        heightLarge,
    )

    boundingQL(
        "bounding-box",
        "BoundingBox",
        owiFile,
        htmlDict,
        outDir,
        lon_0,
        lat_0,
        width,
        height,
    )

    for var in mainVars + detailedVars:
        logger.debug("Generating QL for var %s..." % var)

        detailed = "normal"
        if var in detailedVars:
            detailed = "detailed"

        if var not in owiData:
            if "\n" in var:
                htmlDict[detailed][var] = {}
            else:
                logger.warning("%s is not a valid var name" % var)
            continue

        varName = var
        groupName = os.path.dirname(var)

        data = np.ma.array(owiData[var], mask=mask, dtype=np.float64)
        dataMasked = np.ma.array(owiData[var], mask=mask)
        data = np.ma.filled(data, np.nan)
        vmax = np.nanpercentile(data, 99.5)
        headaxislength = 4.5
        headlength = 5
        quiverColor = "gray"
        cmap = plt.get_cmap("gray")

        varDir = None
        if "owiPreProcessing/estimWindDir_2_cross" in var:
            varDir = var
            varName = var
            var = "owiPreProcessing/ND_2_cross"
            cmap = plt.get_cmap("gray")
            data = np.ma.array(owiData[var], mask=mask)
            dataMasked = np.ma.array(owiData[var], mask=mask)
            data = np.ma.filled(data, np.nan)
            vmax = np.nanpercentile(data, 99.5)
            headaxislength = 0
            headlength = 0
            quiverColor = "blue"
        elif "owiPreProcessing/estimWindDir_2" in var:
            varDir = var
            varName = var
            var = "owiPreProcessing/ND_2"
            cmap = plt.get_cmap("gray")
            data = np.ma.array(owiData[var], mask=mask)
            dataMasked = np.ma.array(owiData[var], mask=mask)
            data = np.ma.filled(data, np.nan)
            vmax = np.nanpercentile(data, 99.5)
            headaxislength = 0
            headlength = 0
            quiverColor = "blue"
        elif var == "owiPreProcessing/wind_streaks_orientation":
            varDir = var
            varName = var
            var = "owiPreProcessing/ND_2"
            cmap = plt.get_cmap("gray")
            data = np.ma.array(owiData[var], mask=mask)
            dataMasked = np.ma.array(owiData[var], mask=mask)
            data = np.ma.filled(data, np.nan)
            vmax = np.nanpercentile(data, 99.5)
            headaxislength = 0
            headlength = 0
            quiverColor = "blue"

        if "WindSpeed" in var:
            cmap = plt.get_cmap("high_wind_speed")
            varDir = var.replace("WindSpeed", "WindDirection")
            vmax = 80
            if not varDir in owiData:
                varDir = None

        if groupName == "":
            groupName = "main"

        htmlDict[detailed][varName] = {}

        cleanVar = varName.replace("/", "-")
        pngFile = "%s/%s-%s.png" % (
            outDir,
            os.path.splitext(os.path.basename(owiFile))[0],
            cleanVar,
        )

        htmlDict[detailed][varName]["png"] = os.path.basename(pngFile)  # toUrl(pngFile)
        htmlDict[detailed][varName]["title"] = varName.replace("%s/" % groupName, "")

        # cmap.set_bad(color='red')

        fig = plt.figure()
        try:
            figtitle = owiMeta[varName]["description"]
            if figtitle is None:
                figtitle = " "
        except:
            figtitle = " "
        plt.title(textwrap.fill(figtitle, 50), fontsize=10)
        #
        cbar_label = owiMeta[var]["units"]
        if cbar_label is None:
            cbar_label = owiMeta[var]["flag_meanings"]

        if cbar_label is not None and len(cbar_label) > 40:
            cbar_label = cbar_label[0:40] + "..."

        logger.debug("Creating map...")
        logger.debug("width %s height %s" % (width, height))

        fig, ax = niceMap(
            projection="stere", lon_0=lon_0, lat_0=lat_0, width=width, height=height
        )

        logger.debug("Map created.")
        logger.debug("Plotting...")
        if varDir:
            if hasNan and not hasGap:
                cs = ax.pcolor(
                    owiLonMask,
                    owiLatMask,
                    dataMasked,
                    vmin=0,
                    vmax=vmax,
                    cmap=cmap,
                    transform=ccrs.PlateCarree(),
                )
            elif hasGap:
                x = owiLonMask.compressed()
                y = owiLatMask.compressed()
                cs = ax.hexbin(
                    x,
                    y,
                    C=dataMasked.compressed(),
                    gridsize=500,
                    vmin=0,
                    vmax=vmax,
                    cmap=cmap,
                    transform=ccrs.PlateCarree(),
                )
            else:
                cs = ax.pcolormesh(
                    owiLon,
                    owiLat,
                    dataMasked,
                    vmin=0,
                    vmax=vmax,
                    cmap=cmap,
                    transform=ccrs.PlateCarree(),
                )

            u, v = polar2uv(data, owiData[varDir])
            ax.quiver(
                owiLon[::20, ::20],
                owiLat[::20, ::20],
                u[::20, ::20],
                v[::20, ::20],
                alpha=0.2,
                headaxislength=headaxislength,
                headlength=headlength,
                color=quiverColor,
                transform=ccrs.PlateCarree(),
            )

            cbar = plt.colorbar(cs, label=cbar_label)
        else:
            nancount = np.count_nonzero(np.isnan(owiData[var]))
            logger.debug(f"{var} vmax={vmax:.3f} nancount={nancount}/{np.size(data)}")
            if hasNan and not hasGap:
                cs = ax.pcolor(
                    owiLonMask,
                    owiLatMask,
                    dataMasked,
                    vmin=0,
                    vmax=vmax,
                    cmap=cmap,
                    transform=ccrs.PlateCarree(),
                )
            elif hasGap:
                x = owiLonMask.compressed()
                y = owiLatMask.compressed()
                try:
                    cs = ax.hexbin(
                        x,
                        y,
                        C=dataMasked.compressed(),
                        gridsize=500,
                        vmin=0,
                        vmax=vmax,
                        cmap=cmap,
                        transform=ccrs.PlateCarree(),
                    )
                except IndexError:
                    logger.error(
                        f"Unable to use hexbin for var {var}, falling back to pcolormesh"
                    )
                    cs = ax.pcolormesh(
                        owiLon,
                        owiLat,
                        dataMasked,
                        vmin=0,
                        vmax=vmax,
                        cmap=cmap,
                        transform=ccrs.PlateCarree(),
                    )
            else:
                cs = ax.pcolormesh(
                    owiLon,
                    owiLat,
                    dataMasked,
                    vmin=0,
                    vmax=vmax,
                    cmap=cmap,
                    transform=ccrs.PlateCarree(),
                )

            cbar = plt.colorbar(cs, label=cbar_label)

        plt.tight_layout()
        fig.tight_layout()
        logger.debug("Finished plotting.")

        logger.debug("Saving into %s" % pngFile)
        plt.savefig(pngFile, dpi=hiDpi, bbox_inches="tight")
        ioBytes = io.BytesIO()
        plt.savefig(ioBytes, format="png", dpi=loDpi, bbox_inches="tight")
        ioBytes.seek(0)
        htmlDict[detailed][varName]["png_small"] = (
            b"data:image/png;base64, " + base64.b64encode(ioBytes.read())
        ).decode("utf-8")
        # plt.show()
        # pdb.set_trace()
        plt.close()
        logger.debug("Finished saving and plotting for var %s" % varName)

    logger.info("Creating HTML file...")

    t = jinja2.Template(template)
    html = t.render(htmlDict=htmlDict, infoDict=infoDict, title=title)
    reportFname = "%s/index.html" % (outDir)
    reportFid = open(reportFname, "w")
    reportFid.write(html)
    reportFid.close()
    logger.info("HTML file created: %s" % toUrl(reportFname, schema="dmz"))
    # pdb.set_trace()
