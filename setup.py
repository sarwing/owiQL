from setuptools import setup, find_packages

setup(
    name="owiQL",
    description="Sarwing owi files visualisation module",
    url="https://gitlab.ifremer.fr/sarwing/owiQL.git",
    use_scm_version={"write_to": "%s/_version.py" % "owiQL"},
    setup_requires=["setuptools_scm"],
    author="Theo Cevaer",
    author_email="Theo.Cevaer@ifremer.fr",
    license="GPL",
    packages=find_packages(),
    install_requires=[
        "numpy",
        "jinja2",
        "shapely",
        "netCDF4",
        "matplotlib",
        "future",
        "cartopy",
        "owi @ git+https://gitlab.ifremer.fr/sarwing/owi.git",
        "colormap_ext @ git+https://gitlab.ifremer.fr/sarwing/colormap_ext.git",
        "pathurl @ git+https://gitlab.ifremer.fr/sarwing/pathurl.git",
        "owi @ git+https://gitlab.ifremer.fr/sarwing/owi.git",
    ],
    zip_safe=False,
    scripts=[
        "bin/swQLWrapper.sh",
        "bin/swQLWrapper2.sh",
        "bin/swQL.py",
        "bin/swMultiResReport.py",
        "bin/swMultiResQLWrapper.sh",
    ],
)
